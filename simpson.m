function integral = simpson(points,mesh)
    if mod(mesh.n,2) == 0
        error("The mesh must have an odd number of nodes in either direction")
        return
    end

    simpsonAxis = ones(mesh.n,1);
    for i = 2:mesh.n-1
        if mod(i,2) == 1
            simpsonAxis(i) = 2;
        else
            simpsonAxis(i) = 4;
        end
    end
    simpson = simpsonAxis.*transpose(simpsonAxis);

    integral = sum(sum(simpson .* points)) * (mesh.h^2)/9;
end
