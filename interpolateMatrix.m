function [expandedMatrix,mesh] = interpolateMatrix(inMatrix,mesh)
    en = 2*mesh.n-1;
    expandedMatrix = zeros(en);
    for i = 1:mesh.n
        for j = 1:mesh.n
            expandedMatrix((2*i-1),(2*j-1)) = inMatrix(i,j);
        end
    end

    for j = 1:en
        if mod(j,2) == 1
            for i = 2:2:en-1
                expandedMatrix(i,j) = avgz(expandedMatrix(i+1,j),expandedMatrix(i-1,j));
            end
        else
            for i = 1:2:en
                expandedMatrix(i,j) = avgz(expandedMatrix(i,j+1),expandedMatrix(i,j-1));
            end
        end
    end
   
    for j = 2:2:en-1
        for i = 2:2:en-1
            expandedMatrix(i,j) = avgz(expandedMatrix(i+1,j),expandedMatrix(i-1,j));
        end
    end

    mesh.h = mesh.h/2;
    mesh.n = en;
end

function outz = avgz(z1,z2)
    outz = (z2-z1)/2 + z1;
end
