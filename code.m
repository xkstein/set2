function code(n)
    h = 1/(n+1);
    nt = (n+1)^2*2;
    ntrow = 2*(n+1);
    K = (h^2)/2;

    mesh.n = n;
    mesh.h = h;

    u = @(x,y) sin(pi*x).*sin(pi*y);

    %k = @(x,y) 1/(1+10*x^2+10*y^2);
    %B = 0;
    %f = @(x,y) 20*((pi*(x^2 + y^2 + 1/10)*sin(pi*y) + cos(pi*y)*y)*sin(pi*x) + cos(pi*x)*sin(pi*y)*x)*pi/(10*x^2 + 10*y^2 + 1)^2;

    k = @(x,y) 1;
    B = 1;
    f = @(x,y) (2*pi^2+1)*sin(pi*x)*sin(pi*y);

    A = sparse(n^2,n^2);
    b = zeros([n^2 1]);

    axySys = [1 0 0
             1 -h 0
             1 0 h];
    phiGradUnprocessed = inv(axySys);
    phiGrad = transpose(phiGradUnprocessed(2:3,:));

    localR = [1/6 1/12 1/12
              1/12 1/6 1/12
              1/12 1/12 1/6]*K;

    localA = zeros(3);
    for i = 1:3
        for j = 1:3
            localA(i,j) = dot(phiGrad(i,:),phiGrad(j,:));
        end
    end

    localb = zeros([3 1]);

    % This is the greater loop which covers all the triangles and populates the stiffness and mass matricies, as well as the load vector
    for tri = 1:nt
        % This sets the positions of the nodes for the two possible triangle orientations
        if mod(tri,2)
            x = h * mod(tri-1,ntrow)/2;
            y = h * floor((tri-1)/ntrow);

            localb(1)=f(x,y+h);
            localb(2)=f(x,y);
            localb(3)=f(x+h,y+h);

            x = h/3 + x;
            y = 2*h/3 + y;
        else
            x = h * mod(tri-2,ntrow)/2;
            y = h * floor((tri-1)/ntrow);

            localb(1)=f(x+h,y);
            localb(2)=f(x+h,y+h);
            localb(3)=f(x,y);

            x = 2*h/3 + x;
            y = h/3 + y;
        end

        % This loop populates the load vector
        localb = localb/3 * K;
        for i = 1:3
            node = loc2glo(tri,i,n);
            if (node > 0)
                b(node) = b(node) + localb(i);
            end
        end

        % This loop is for populating the stiffness matrix
        for i = 1:3
            for j = 1:3
                node1 = loc2glo(tri,i,n);
                node2 = loc2glo(tri,j,n);
                if (~node1 || ~node2)
                    continue
                end
                A(node1,node2) = A(node1,node2) + (localA(i,j) * K * k(x,y)) + (B * localR(i,j));
            end
        end
    end

    ksi = A\b;

    X = (1:n)*h;
    KSI = zeros(n);
    for i = 1:n
        j = n * (i-1);
        KSI(i,:) = transpose(ksi(j+1:j+n));
    end

    [L2Error,H1Error] = findError(KSI,mesh);
    fprintf('| 1/%i | %f | %f |\n',n+1,L2Error,H1Error);

    %surf(X,X,KSI)
    clear 
end
