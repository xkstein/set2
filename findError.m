function[L2Error,H1Error] = findError(KSI,mesh)

    u = @(x,y) sin(pi*x).*sin(pi*y);
    dux = @(x,y) pi*cos(pi*x).*sin(pi*y);
    duy = @(x,y) pi*sin(pi*x).*cos(pi*y);

    [ksi,expandedMesh] = interpolateMatrix(KSI,mesh);
    [KSIy,KSIx] = gradient(ksi,expandedMesh.h);

    u_uh = zeros(mesh.n);
    u_uhx = zeros(expandedMesh.n);
    u_uhy = zeros(expandedMesh.n);
    du_uh = zeros(expandedMesh.n);

    for j = 1:mesh.n
        y = j * mesh.h;
        for i = 1:mesh.n
            x = i * mesh.h;

            u_uh(i,j) = u(x,y) - KSI(i,j);
        end
    end

    for j = 1:expandedMesh.n
        y = j * expandedMesh.h;
        for i = 1:expandedMesh.n
            x = i * expandedMesh.h;

            %u_uh(i,j) = u(x,y) - KSI(i,j);
            u_uhx(i,j) = dux(x,y) - KSIx(i,j);
            u_uhy(i,j) = duy(x,y) - KSIy(i,j);
        end
    end

    L2Semi = simpson((u_uh.^2),mesh);
    H1Semi = simpson((u_uhx.^2 + u_uhy.^2),expandedMesh);
    L2Error = sqrt(L2Semi);
    H1Error = sqrt(L2Semi + H1Semi);
end

